//console.log("Hello World");

// Array Methods
/*
	for array methods, we have the mutator and non-mutatot methods.
*/

// MUTATOR METHODS
/*
	- are functions that mutate or change an array after they are created.
	- these methods manipulate the original array, performing such as adding and removing  elements
	- push(), pop(), unshift(), shift(), splice(), sort(), reverse()
*/

console.log("MUTATOR METHODS");

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruit'];
console.log(fruits);

//push()
/*
	- adds in the end of the array and returns the array's length
	- [0, 0, "x"];
	- syntax:
	arrayName.push();
*/
console.log("///// PUSH METHOD /////");
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method");
console.log(fruits);

//push multiple elements to an array

fruits.push('Avocado', 'Guava');
console.log("Mutated array from the push method:");
console.log(fruits);
console.log('');


//pop()
/*
	- removes the last element in an array and returns the removed element
	- [0, 0, "x"];
	- syntax:
		arrayName.pop();
*/
console.log("///// POP METHOD /////");
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from the pop method:");
console.log(fruits);
console.log('');


// unshift()
/*
	- adds at the start of an array
	- ["x", 0, 0];
	- syntax:
		arrayName.unshift('element');
		arrayName.unshift('elementA', elementB);
*/
console.log("///// UNSHIFT METHOD /////");
fruits.unshift('Lime', 'Banana');
let trialUnshift = fruits.unshift('Lime', 'Banana');
console.log(trialUnshift);
console.log("Mutated array from the unshift method:");
console.log(fruits);
console.log('');


// shift()
/*
	- removes element at the sart of the array, and returns the removed element
	- syntax:
		arrayName.shift();
*/
console.log("///// SHIFT METHOD /////");
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from the shift method:");
console.log(fruits);
console.log('');


//splice()
/*
	- simultaneosly removes element from a specified number and adds element
	- syntax:
		arrayName.splice(startingIndex, deleteCount, elementToBeAdded)

		arrayName.splice(whereToStartDeleting, numberOfItemsToBeDeleted, elementsToBeDeleted);
*/
console.log("///// SPLICE METHOD /////");
fruits.splice(1, 2, 'Mango', 'Cherry');
console.log("Mutated array from the splice method:");
console.log(fruits);
console.log('');


// sort()
/*
	- rearranges the array elements in an alphanumeric order
	- syntax:
		arrayName.sort();
*/
console.log("///// SORT METHOD /////");
fruits.sort();
console.log("Mutated array from the sort method:");
console.log(fruits);
console.log('');


// reverse()
/*
	- reverses the order of the elements in the array
	- syntax:
		arrayName.reverse();
*/
console.log("///// REVERSE METHOD /////");
fruits.reverse();
console.log("Mutated array from the reverse method:");
console.log(fruits);
console.log('');
console.log("END OF MUTATOR METHODS");