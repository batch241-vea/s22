//console.log("Hello");

// NON-MUTATOR METHODS
/*
	- are functions that do not mofidy or change an array after they have been created
	- these methods do not manipulate the original array
	- these methods perform task such as returning(or getting) elements from an array, combining arrays, and printing the output.
	- indexOf(), lastIndexOf(), slice(), toString(), concat(), join()
*/
console.log("NON-MUTATOR METHOD");

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries);
console.log("");

// indexOf() method
/*
	- returns the index number of the first matching element founf in the array
	 - of no mathc has been found, the result will be -1
	 - the search process will be done from the first element proceeding to the last element.
	 - syntax:
	 	arrayName.indexOf(searchValue);
	 	arrayName.indexOf(searchValue, fromIndex);
*/
console.log("///// indexOf METHOD /////");
let firstIndex = countries.indexOf('PH');
console.log(firstIndex);
console.log("Result of indexOf method is " + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log(invalidCountry);
console.log("Result of invalid indexOf method is " + invalidCountry);
console.log('');


// lastIndexOf() method
/*
	- returns the index number of the last matching item found in the array
	- the search process will be done from the last element proceeding to the first element.
	- syntax: 
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, fromIndex);
*/
//getting the index number starting from the last index
console.log("///// lastIndexOf METHOD /////");
let lastIndex = countries.lastIndexOf('PH');
console.log(lastIndex);
console.log("Result of indexOf method is " + lastIndex);

//getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf("PH", 6);
console.log("Result of indexOf method is " + lastIndexStart);

let lastIndexStarter = countries.lastIndexOf("PH", 2);
console.log("Result of indexOf method is " + lastIndexStarter);
console.log('');


// slice() method
/*
	- slices elements from an array and returns a new array
	- syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/
console.log("///// SLICE METHOD /////");

// slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log("Result of slice method is " + slicedArrayA);

//Slicing off elements from a specified index to another index
letslicedArrayB = countries.slice(4, 6);
console.log("Return from slice method is " + letslicedArrayB);

//Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-4, -1);
let slicedArrayD = countries.slice(-4);
let slicedArrayE = countries.slice(-6);
console.log("Return from slice method is");
console.log(slicedArrayC);
console.log("Return from slice method is");
console.log(slicedArrayD);
console.log("Return from slice method is");
console.log(slicedArrayE);
console.log('');


//	toString() method
/*
	- returns an array as a string separated by commas(,)
	- syntax:
		arrayName.toString();
*/
console.log("///// toString METHOD /////");

let stringArray = countries.toString();
console.log("Result of toString method");
console.log(stringArray);


// concat() method
/*
	- combines two arrays and returns the combined result
	- syntax:
		arrayA.concat(arrayB);
		arrayA.concat(ElementA, ElementB);
*/
console.log("///// concat METHOD /////");

let tasksArrayA = ["drink HTML", "eat Javascript"];
let tasksArrayB = ["inhale CSS", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result of toString method");
console.log(tasks);

let allTasks = tasks.concat(tasksArrayC);
console.log("Result of toString method");
console.log(allTasks);

//combining arrays with elements
let combinedTasks = tasksArrayA.concat("smell espress", "throw react");
console.log("Result of toString method");
console.log(combinedTasks);
console.log('');


// join() method
/*
	- returns an array as a string separated by specified separator string
	- syntax:
		arrayName.join("separatorString");
*/
console.log("///// join METHOD /////");

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(' - '));
console.log('');


console.warn("END OF NON MUTATOR METHODS")
console.log('');





console.log("/////// ITERATOR METHODS /////////");
console.log('');
/*
	- ITERATOR METHODS are loops designed to perform repititive tasks on arrays
	- useful for manipulating array data resulting in complex tasks.
*/

// foreach() method
/*
	- similar to for loop that iterates on each array element
	- it is a common practice to use the singular form of the array content for parameter names used in array loops.
	- Array iteratOR methods normally work with a function supplied as an argument
	- syntax:
		arrayName.forEach(function(individualElement) (statement))
*/
console.log("///// forEach METHOD /////");

allTasks.forEach(function(task){
	console.log(task)
});

let students = ['Jeru', 'Annejanette', 'Glyn', 'Jake', 'Gabryl', 'Daniel'];

students.forEach(function(student){
	console.log("Hi!" + student);
});


//using foreach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	if (task.length >= 10){
		console.log(task);
		filteredTasks.push(task);
	}
})
console.log("Result of filteredTasks");
console.log(filteredTasks);

students.forEach(function(student){
	if (student.length <= 4){
		console.log("Sit down " + student);
	} else{
		console.log("Stand up " + student);
	}
})
console.log('');


// map() method
/*
	- it iterates each element and returns new array with different values depending on the result of the function's operation
	- this is useful for performing tasks where mutation/changing the elemetns are required.
	- unlike the firEach methods, the map methods requires the use of return statement in order to create another array with the performed operation
	- syntax:
		let/const resultArray = arrayName.map(function(individualElement){statement})
*/
console.log("///// map METHOD /////");

let numbers = [1, 2, 3, 4, 5];
console.log("Before the map method: " + numbers);

let numberMap = numbers.map(function(number){
	return number * number
})
console.log("Result of the map method:");
console.log(numberMap);
console.log('');


// every() method
/*
	- checks if all element in an array meet the given condition
	- this is useful for validating data stored in arrys especially when dealing with large amounts of data.
	- returns the true value if all elements meet the condition and false if otherwise.
	- syntax:
		let/const resultArray = arrayName.very(function(element){
		return expression/condition
		});
*/
console.log("///// every METHOD /////");

let allValid = numbers.every(function(number){
	return (number < 3);
});
console.log("Result of the every method:");
console.log(allValid);
console.log('');


// some() method
/*
	- checks if atleast one element in the array meets the given condition
	- returns a true value if atleast one element meets the condition and false if otherwise
	- syntax:
		let/const resultArray = arrayName.some(functionName(element){
		return expression/condition;
		});
*/

console.log("///// every METHOD /////");

let someValid = numbers.some(function(number){
	return (number < 2);
});
console.log("Result of the some method:");
console.log(someValid);
console.log('');

// combining the returned result from the every/some methods may be used in other statements to perform censecutive

if (someValid){
	console.log("Some numbers in the array are greater than 2")
}
console.log('');


// filter() method
/*
	- returns new array that contains elements which meets the given condition
	- returns an empty array if no elements are found
	- useful for filtering array elements with a given condition and shortens the syntax compared to using other array iterator methods.
	- syntax:
		let/const resultArray = arrayName.filter(functionName(element){
		return expression/condition
		});
*/
console.log("///// filter METHOD /////");

let filterValid = numbers.filter(function(number){
	return number < 3;
});
console.log("Result of the filter method:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number = 0);
})
console.log(nothingFound);
console.log('');

//filtering using forEach
let filteredNumbers = [];

numbers.forEach(function(number){
	console.log(number);

	if (number < 3){
		filteredNumbers.push(number)
	}
});
console.log("Result of the filter method:");
console.log(filteredNumbers);
console.log('');


//includes() method
/*
	- methods can be "chained" by using them one after another
	- the result of the first method is used on the second method until all chained methods have been solved.
*/
console.log("///// includes METHOD /////");

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
});
console.log("Result of the includes method:");
console.log(filteredProducts);
console.log('');


// reduce() method
/*
	- evaluates statements from left to right and returns/reduces the array into a single value.
	- syntax:
		let/const resultArr = arrayName.reduce(function(accumulator, currentValue){
			return expression/condition
		})
	- the accumulator parameter in the function stores the result for every iteration of the loop
	- the currentValue is a current/next element in the array that is evaluated in each iteration of the loop
	- how te reduce method works:
		1. fibonacci sequence
*/
console.log("///// reduce METHOD /////");
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current Iteration: " + iteration);
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);

	return x + y;
});
console.log("Result of the reduce method:");
console.log(reducedArray);


// reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y){
	return x + " " + y;
});

console.log("Result of the reduce method: " + reducedJoin);